# addition
# Align binary points
# Add significands
# Normalize result
import math
import struct
def make_it_bianry(number_to_transform, bits_shown):
    s = bin(number_to_transform & int("1" * bits_shown, 2))[2:]
    return ("{0:0>%s}" % bits_shown).format(s)

def make_binary_sum(input_array, we_need_string, if_output_array_in_string):
    max_len = max(len(x) for x in input_array)
    input_array = [x.zfill(max_len) for x in input_array]
    result = [0] * (max_len + 1)
    for i in range(max_len - 1, -1, -1):
        column_sum = sum(int(x[i]) for x in input_array) + result[i + 1]
        result[i + 1] = column_sum % 2
        result[i] += column_sum // 2
    if we_need_string:
        result = "".join(str(x) for x in result).lstrip("0")
        return result
    else:
        if if_output_array_in_string:
            string_result = []
            for i in result:
                string_result.append(str(i))
            string_result.pop(0)
            return string_result
        return result

def add_floats(x1, x2):
    # to ieee_754 encoding:
    if not isinstance(x1, float) or not isinstance(x2, float): #or max(x2, x1) > 128:
        print("INPUT INT! ERROR")
        return None

    x_sign_bit = False
    y_sign_bit = False
    if x1 < 0:
        x_sign_bit = True
    if x2 < 0:
        y_sign_bit = True
    x1 = abs(x1)
    x2 = abs(x2)
    binary_len_set = 16

    binary_x_exponent = make_it_bianry(math.floor(x1),binary_len_set).lstrip('0')#[::-1]
    binary_y_exponent = make_it_bianry(math.floor(x2),binary_len_set).lstrip('0')#[::-1]

    binary_x_exponent = binary_x_exponent[1:len(binary_x_exponent)]
    binary_y_exponent = binary_y_exponent[1:len(binary_y_exponent)]

    mantissa_x_binary = binary_x_exponent
    mantissa_y_binary = binary_y_exponent


    one_hundred_twenty_seven_binary = make_it_bianry(127, 8)
    binary_x_exponent = make_it_bianry(len(binary_x_exponent),8)
    binary_y_exponent = make_it_bianry(len(binary_y_exponent),8)



    if x_sign_bit == 0 or x_sign_bit == 1:
        binary_x_exponent = make_binary_sum([binary_x_exponent, one_hundred_twenty_seven_binary], True, True)
    if y_sign_bit == 0 or y_sign_bit == 1:
        binary_y_exponent = make_binary_sum([binary_y_exponent, one_hundred_twenty_seven_binary], True, True)

    float_len_x = str(x1)
    float_len_x = float_len_x.split('.')
    float_len_y = str(x2)
    float_len_y = float_len_y.split('.')

    mantissa_x = x1 - math.floor(x1)
    mantissa_y = x2 - math.floor(x2)

    round_arrange = (max(len(float_len_x[1]),len(float_len_y[1])))

    for i in range(1,24 - len(mantissa_x_binary)):
        x_mn_calc = round((mantissa_x * 2), round_arrange)
        mantissa_x_binary += str(x_mn_calc)[0]
        mantissa_x = float("0." + str(x_mn_calc).split('.')[1])


    for i in range(1, 24 - len(mantissa_y_binary)):
        y_mn_calc = round((mantissa_y * 2), round_arrange)
        mantissa_y_binary += str(y_mn_calc)[0]
        mantissa_y = float("0." + str(y_mn_calc).split('.')[1])

    status_x = ''
    status_y = ''
    if x_sign_bit == 1:
        status_x = '-'
    if y_sign_bit == 1:
        status_y = '-'

    b1 = (f"{int(x_sign_bit)}{binary_x_exponent}{mantissa_x_binary}")
    b2 = (f"{int(y_sign_bit)}{binary_y_exponent}{mantissa_y_binary}")

    s1, e1, m1 = int(b1[0]), int(b1[1:9], 2), int(b1[9:], 2)
    s2, e2, m2 = int(b2[0]), int(b2[1:9], 2), int(b2[9:], 2)



    # Визначення більшого за абсолютним значенням числа та різниці експонент
    if abs(x1) < abs(x2):
        x1, x2 = x2, x1
        s1, s2 = s2, s1
    exp_diff = e1 - e2

    # Зсув мантиси меншого числа вліво на різницю експонент
    m2 <<= exp_diff

    # Додавання мантис двох чисел
    if s1 == s2:
        m3 = m1 + m2
    else:
        m3 = m1 - m2



    if m3 >= 2 ** 23:
        e3 = e1 + 1
        m3 >>= 1
    else:
        e3 = e1
    m3 &= (2 ** 23 - 1)

    # Перетворення бінарного формату назад у десятковий
    b3 = format((s1 << 31) | (e3 << 23) | m3, '032b')
    f3 = struct.unpack('!f', struct.pack('!I', int(b3, 2)))[0]
    print('Бінарне представлення')
    print(b3)
    print('Представлення у десятковій формі')
    print(f3)

num1 = float(input("Enter first number: "))
num2 = float(input("Enter second number: "))
add_floats(num1, num2)