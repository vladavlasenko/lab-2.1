#2.2 Зсув залишку вправо


import math

def division(num1, num2):
    divider = int(num1)  # перетворити перший параметр на ціле число та призначити його "діленому"
    divisor = int(num2)  # перетворити другий параметр на ціле число та призначити його "дільнику"
    register = divider  # призначити «ділене» для «реєстру»
    quotient = int(math.pow(2, 32) - 1)  # обчислення максимального значення для 32-розрядних цілих чисел
    remainder_length = 32  # довжину залишку
    quotient_length = 32  # бітова довжина частки
    register_length = 64  # довжина біта регістра

    print("\nNumbers in binary:")
    print("Divider:\t" + finish_string_with_zeros(bin(divider)[2:], remainder_length)) #двійкове представлення дільника
    print("Divisor:\t" + finish_string_with_zeros(bin(divisor)[2:], remainder_length))

    subtract_divisor = -(divisor << 32) # зсув дільника вліво на 32 біта
    divisor <<= 32

    for i in range(32):
        print("\nStep №" + str(i+1))  # номер поточного кроку
        print("Register:\t" + finish_string_with_zeros(bin(register)[2:], register_length)) # двійкове представлення реєстру
        register <<= 1  # зсув регістру вліво на 1 біт
        print("Зсуваємо регістр вліво на один біт;")

        if register >= divisor:
            print("Remainder більший(або рівний) ніж Divisor")

            register += subtract_divisor # відняти ділене з регістру
            register |= 1  # присвоєння молодшого біту регістра на 1
            print("Register:\t" + finish_string_with_zeros(bin(register)[2:], register_length)) #двійкове представлення реєстру
        else: # якщо регістр менший від дільника
            print("Remainder менший ніж Divisor")

            print("Register:\t" + finish_string_with_zeros(bin(register)[2:], register_length))

    print("\nAnswer:")
    print("Quotient:\t" + finish_string_with_zeros(bin(register & quotient)[2:], quotient_length) +
          " ( " + str(register & quotient) + " )")
    print("Remainder:\t" + finish_string_with_zeros(bin(register >> 32)[2:], remainder_length) +
          " ( " + str(register >> 32) + " )")

def finish_string_with_zeros(val, bitscount):
    count = bitscount - len(val)
    head = ""
    for i in range(count):
        head += "0"
    return head + val

num1 = input("Введіть перше число: ")
num2 = input("Введіть друге число: ")
division(num1, num2)