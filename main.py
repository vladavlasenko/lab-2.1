#1.2 зсув результату вправо

# функція для перетворення десяткового числа в двійкове
def dec_to_bin(n):
    return bin(n).replace("0b", "")

# ввід множників у десятковому форматі
a = int(input("Введіть перше число: "))
b = int(input("Введіть друге число: "))

# перетворення множників в двійкову форму
bin_a = dec_to_bin(a)
bin_b = dec_to_bin(b)

# встановлення розміру результату
result_size = len(bin_a) + len(bin_b)

# створення масиву з результатом заповненим нулями
result = [0] * result_size

# множення двійкових чисел
for i in range(len(bin_a)):
    for j in range(len(bin_b)):
        if bin_a[i] == "1" and bin_b[j] == "1":
            result[i+j] += 1

# зсув результату вправо на один розряд
for i in range(result_size-1, 0, -1):
    result[i-1] += result[i] // 2
    result[i] = result[i] % 2

# вивід результату
print(f"Результат множення із зсувом {bin_a} і {bin_b} = {''.join(map(str,result))}")